<?php

namespace App\Repository;

use App\Entity\Planette;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Planette|null find($id, $lockMode = null, $lockVersion = null)
 * @method Planette|null findOneBy(array $criteria, array $orderBy = null)
 * @method Planette[]    findAll()
 * @method Planette[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanetteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Planette::class);
    }

    // /**
    //  * @return Planette[] Returns an array of Planette objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Planette
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
